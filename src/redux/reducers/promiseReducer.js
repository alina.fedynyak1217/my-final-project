import {createGql, actionLogin, actionOnLogin, actionGetCatagories} from './actions.js'

let promiseReducer = (state, action) => {

    const actions = {
        PROMISE() {
            const {status, name, payload, error} = action
            return {
                ...state,
                [name]: {status, payload, error}
            }
        }
    }
    if (state === undefined) {
        return {}
    }
    if (action.type in actions) {
        return actions[action.type]()
    }
    return state
}

function actionPromise(name, promise) {
    const actionPending = () => ({
        type: 'PROMISE',
        status: 'PENDING',
        payload: null, name,
        error: null
    })
    const actionResolved = payload => ({
        type: 'PROMISE',
        status: 'RESOLVED', payload, name,
        error: null
    })
    const actionRejected = error => ({
        type: 'PROMISE',
        status: 'REJECTED',
        payload: null, name, error
    })

    return async dispatch => {
        dispatch(actionPending())
        try {
            let payload = await promise
            dispatch(actionResolved(payload))
        } catch (e) {
            dispatch(actionRejected(e))
        }
    }
}

function actionDeletePromise(name, promise) {
    const actionPending = () => ({
        type: 'PROMISE',
        status: 'DELETED',
        payload: null, name,
        error: null
    })

    return dispatch => {
        dispatch(actionPending())
    }
}

function actionPromiseLogin(login, passw) {
    const name = 'LOGIN'
    const promise = createGql().request(`query login ($login: String!, $passw: String!) {
      login(login: $login, password: $passw)
    }`, {login, passw})
    const actionPending = () => ({
        type: 'PROMISE',
        status: 'PENDING',
        payload: null, name,
        error: null
    })
    const actionResolved = payload => ({
        type: 'PROMISE',
        status: 'RESOLVED', payload, name,
        error: null
    })
    const actionRejected = error => ({
        type: 'PROMISE',
        status: 'REJECTED',
        payload: null, name, error
    })

    return async dispatch => {
        dispatch(actionPending())
        try {
            let payload = await promise
            dispatch(actionResolved(payload))
            payload.login && dispatch(actionLogin(payload.login)) && dispatch(actionGetCatagories())
        } catch (e) {
            dispatch(actionRejected(e))
        }
    }
}

function actionPromiseRegister(login, password) {
    const name = 'REGISTER'
    const promise = createGql().request(`mutation reg($login:String!, $password: String!){
  createUser(login: $login, password:$password){
    _id   }
 }`, {login, password})
    const actionPending = () => ({
        type: 'PROMISE',
        status: 'PENDING',
        payload: null, name,
        error: null
    })
    const actionResolved = payload => ({
        type: 'PROMISE',
        status: 'RESOLVED', payload, name,
        error: null
    })
    const actionRejected = error => ({
        type: 'PROMISE',
        status: 'REJECTED',
        payload: null, name, error
    })

    return async dispatch => {
        dispatch(actionPending())
        try {
            let payload = await promise
            dispatch(actionResolved(payload))
            payload && payload.createUser && payload.createUser._id && dispatch(actionOnLogin(login, password))
        } catch (e) {
            dispatch(actionRejected(e))
        }
    }
}

function actionMutation(name, promise, ...params) {

    const actionPending = () => ({
        type: 'PROMISE',
        status: 'PENDING',
        payload: null, name,
        error: null
    })
    const actionResolved = payload => ({
        type: 'PROMISE',
        status: 'RESOLVED', payload, name,
        error: null
    })
    const actionRejected = error => ({
        type: 'PROMISE',
        status: 'REJECTED',
        payload: null, name, error
    })

    return async dispatch => {
        dispatch(actionPending())
        try {
            let payload = await promise
            dispatch(actionResolved(payload))
        } catch (e) {
            dispatch(actionRejected(e))
        }
    }
}

export {promiseReducer, actionPromise, actionPromiseLogin, actionMutation, actionDeletePromise, actionPromiseRegister}
