import React from 'react';
import { Route, Redirect} from 'react-router-dom';
import {connect}   from 'react-redux';

let PrivateRoute = props =>
<Route {...props}
    component={pageComponentProps => {
        const PageComponent = props.component
        console.log(pageComponentProps)
        if (props.data && props.data.some(r=> props.roles.includes(r)))
            return (
                <PageComponent {...pageComponentProps}/>
            )
        else {
            return (
                <Redirect to={props.fallback} />
            )
        }
    }
}/>

PrivateRoute = connect(state => ({
    data: state && state.token
        && state.token.data
        && state.token.data.sub
        && state.token.data.sub.acl}))
(PrivateRoute)

export {PrivateRoute}
