let goodsCartReducer = (state, action) => {

    const actions = {
        AddGOOD() {
            const {id, count} = action
            return {
                ...state,
                [id]: count
            }
        },
        DelGOOD() {

            delete state[action.id]
            return {
                ...state
            }
        },
        DelCart() {
            return {}
        }
    }

    if (state === undefined) {
        return {}
    }

    if (action.type in actions) {
        return actions[action.type]()
    }

    return state
}

function actionAdd(id, count) {

    const actionAddGood = () => ({type: 'AddGOOD', id, count})

    return dispatch => {
        dispatch(actionAddGood())
    }
}

function actionDel(id) {

    const actionDelGood = () => ({type: 'DelGOOD', id})

    return dispatch => {
        dispatch(actionDelGood())
    }
}

function delCart() {

    const actionDelCart = () => ({type: 'DelCart'})

    return dispatch => {
        dispatch(actionDelCart())
    }
}


export {actionAdd, actionDel, goodsCartReducer, delCart}
