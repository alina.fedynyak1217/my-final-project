import jwt_decode from  'jwt-decode'

const defaultState = {}

let gqlReducer = (state, action) => {
    if (state === undefined) {
        return defaultState
    }
    if (action.type === 'LOGIN') {
        localStorage.authToken = action.token
        console.log("token  " + localStorage.authToken)
        return {token: action.token, data: jwt_decode(action.token)}
    }
    if (action.type === 'LOGOUT') {
        localStorage.removeItem('authToken')
        console.log("token  " + localStorage.authToken)
        return {}
    }
    if (action.type === 'REGISTER') {
        return {}
    }
    return state
}

export {gqlReducer}
