import {actionPromise} from './promiseReducer.js';
import {actionPromiseLogin} from './promiseReducer.js';
import { GraphQLClient } from 'graphql-request'
import {actionMutation, actionDeletePromise} from './promiseReducer.js'

// const gql = new GraphQLClient("/graphql", { headers: {Authorization: "Bearer " + "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOnsiaWQiOiI1ZDliN2FjZTQ2OGVkNjc4MWM1M2VhNDAiLCJsb2dpbiI6Im1ha3N5bSIsImFjbCI6WyI1ZDliN2FjZTQ2OGVkNjc4MWM1M2VhNDAiLCJ1c2VyIl19LCJpYXQiOjE1NzA0NzM4NDF9.ZZ7LB7L02DRANr0YsOshMrmN6o_h4akYIPL_xKZ3hn8"}})
function createGql() {
    let gql
    if (localStorage.authToken) {
        gql = new GraphQLClient("http://shop-roles.asmer.fs.a-level.com.ua/graphql", {headers: {Authorization: "Bearer " + localStorage.authToken}})
    } else {
        gql = new GraphQLClient("http://shop-roles.asmer.fs.a-level.com.ua/graphql", {headers: {}})
    }
    return gql
}

const actionLogin = (token) => ({
    type: 'LOGIN', token
})
const actionLogout = () => ({
    type: 'LOGOUT'
})
const actionOnLogin = (log, passw) => actionPromiseLogin(log, passw)

const actionRegister = (login, password) => actionMutation('REGISTER', createGql().request(`mutation reg ($login: String!, $password: String!){
  UserUpsert(user:{
    login: $login, password: $password
  }){
    _id
  }
}`, {login, password}), login, password)

const actionGetCatagories = () => actionPromise('getCategories', createGql().request(`query categories {
  CategoryFind (query: "[{}]") {
    _id, name, goods{name},image{_id}, subCategories{name, _id}
  }
}`))

const actionGetCatagorieOne = (id) => actionPromise('getCategorieOne', createGql().request(`query categoryFindOne {
  CategoryFindOne(query: "[{\\"_id\\": \\"${id}\\"}]") {
    _id, name, goods{name, _id,  description, price, images{url, _id}}
  }
}`))

const actionGetGoods = () => actionPromise('getGoods', createGql().request(`query good {
  GoodFind(query: "[{}]") {
   name, _id, description, price, images{url, _id}, categories{_id}
  }
}`))

const actionGetOrder = (id) => actionPromise('getOrder', createGql().request(`query ordqueryOne{
  OrderFindOne(query: "[{\\"_id\\": \\"${id}\\"}]"){
     _id, total, owner{login, _id}, createdAt
    orderGoods{
      _id, good{ _id, name, price}, count, total, price
    }
  }
}`))

const actionGetUser = (id) => actionPromise('getUser', createGql().request(`query userFind($q:String) {
  UserFindOne(query: $q){
    _id, login, nick, createdAt
  }
}`, {q: JSON.stringify([{_id: id}])}))

const actionGetOrderHistory = () => actionPromise('getOrderHistory', createGql().request(`query ordquery{
  OrderFind(query: "[{}, {\\"sort\\":[{\\"_id\\": -1}]}]"){
     _id, total, owner{login, _id}, createdAt
    orderGoods{
      _id, good{ _id, name, price}, count, total, price
    }
  }
}`))

const actionGetOrderOne = (id) => actionPromise('getOrderOne', createGql().request(`query ordqueryOne ($querry: String){
  OrderFindOne(query: $querry){
     _id, total, owner{login, _id}, createdAt
    orderGoods{
      _id, good{ _id, name, price}, count, total, price
    }
  }
}`, {querry: JSON.stringify([{_id: id}])}))

const actionDelete = () => actionDeletePromise('sendOrder')
const actionDeleteRegister = () => actionDeletePromise('REGISTER')

function searchRegExp(str) {
    return JSON.stringify([{$or: [{name: "/" + str.trim().split(/\s+/).join('|') + "/"}]}])
}

const actionSearch = (quer) => actionPromise('searchGood', createGql().request(`query goodquerySearch ($query: String){
  GoodFind(query: $query) {
   name, _id,  description, price, images{url, _id}, categories{_id}
  }
}`, {query: searchRegExp(quer)}))


const actionSend = (orderObject) => {
    let orderGoods = []
    for (let key in orderObject) {
        orderGoods.push({
            good: {
                _id: key
            },
            count: orderObject[key]
        })
    }
    let order = {
        orderGoods: orderGoods
    }
    return actionMutation('sendOrder', createGql().request(`mutation order($order:OrderInput) {
                                        OrderUpsert(order: $order) {
                                          _id
                                        }
                                      }`, {order}))
}

const actionAddCategory = (catName) => {
    let category = {
        name: catName
    }
    return actionMutation('addCat', createGql().request(`mutation categoryadd ($category: CategoryInput) {
    CategoryUpsert(category: $category) {
      _id
    }
  }`, {category}))
}

const actionAddGood = (name, description, price, category, categoryId) => {
    let good = {
        name, description, price, categories: {
            _id: categoryId,
            name: category
        }
    }
    return actionMutation('addGood', createGql().request(`mutation goodadd($good: GoodInput) {
  GoodUpsert(good: $good) {
    _id
  }
}`, {good}))
}

const actionAddImage = (imageId, goodId) => {
    let image = {
        _id: imageId,
        good: {
            _id: goodId
        }
    }
    return actionMutation('addGood', createGql().request(`mutation addImage($image: ImageInput) {
  ImageUpsert(image: $image) {
    _id
  }
}`, {image}), imageId, goodId)
}

const actionAddPhotoBack = (form) => actionPromise(
    'UPLOAD',
    fetch('/upload', {
        method: "POST",
        headers: localStorage.authToken ? {
            Authorization: 'Bearer ' + localStorage.authToken
        } : {},
        body: new FormData(form)
    }).then(res => res.json()))

export {actionLogin, actionLogout, actionOnLogin, actionGetCatagories, actionGetGoods, createGql, actionSend, actionGetOrder, actionGetCatagorieOne, actionGetUser, actionGetOrderHistory, actionDelete, actionGetOrderOne, actionRegister, actionSearch, actionAddCategory, actionAddGood, actionAddPhotoBack, actionAddImage, actionDeleteRegister}
