import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {promiseReducer} from "./reducers/promiseReducer.js";
import {gqlReducer} from './reducers/gqlReducer.js'
import {goodsCartReducer} from './reducers/goodsCartReducer.js'
import {actionLogin} from './reducers/actions.js'

const reducers = combineReducers({
    token: gqlReducer,
    promise: promiseReducer,
    addGood: goodsCartReducer
})

const store = createStore(reducers, applyMiddleware(thunk))
if (localStorage.authToken) {
    store.dispatch(actionLogin(localStorage.authToken))
}

store.subscribe(() => console.log(store.getState()))

export default store
