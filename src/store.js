import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import { GraphQLClient } from 'graphql-request'
import jwt_decode from  'jwt-decode'

let promiseReducer = (state, action) => {
  const actions = {
    PROMISE () {
      const {status, name, payload, error} = action
      return {
        ...state,
        [name]: {status, payload, error}
      }
    }
  }
  if (state === undefined){
      return {}
  }
  if (action.type in actions) {
    return actions[action.type]()
  }
  return state
}

function actionPromise (name, promise) {
  const actionPending = () => ({ type: 'PROMISE', status: 'PENDING', payload: null,name, error: null })
  const actionResolved = payload => ({ type: 'PROMISE', status: 'RESOLVED', payload,name, error: null })
  const actionRejected = error => ({ type: 'PROMISE', status: 'REJECTED', payload: null,name, error })

  return async dispatch => {
    dispatch(actionPending())
    try {
      let payload = await promise
      dispatch(actionResolved(payload))
    }
    catch (e) {
      dispatch(actionRejected(e))
    }
  }
}

function actionPromiseLogin (login, passw) {
  const name = 'LOGIN'
  const promise = gql.request(`query login ($login: String!, $passw: String!) {
      login(login: $login, password: $passw)
    }`, {login, passw})
  const actionPending    = () => ({ type: 'PROMISE', status: 'PENDING', payload: null,name, error: null })
  const actionResolved    = payload => ({ type: 'PROMISE', status: 'RESOLVED', payload,name, error: null })
  const actionRejected    = error => ({ type: 'PROMISE', status: 'REJECTED', payload: null,name, error })

  return async dispatch => {
    dispatch(actionPending())
    try {
      let payload = await promise
      dispatch(actionResolved(payload))
      payload.login && dispatch(actionLogin(payload.login))
    }
    catch (e) {
      dispatch(actionRejected(e))
    }
  }
}


const gql = new GraphQLClient("/graphql", { headers: {Authorization: "Bearer " + "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOnsiaWQiOiI1ZDliN2FjZTQ2OGVkNjc4MWM1M2VhNDAiLCJsb2dpbiI6Im1ha3N5bSIsImFjbCI6WyI1ZDliN2FjZTQ2OGVkNjc4MWM1M2VhNDAiLCJ1c2VyIl19LCJpYXQiOjE1NzA0NzM4NDF9.ZZ7LB7L02DRANr0YsOshMrmN6o_h4akYIPL_xKZ3hn8"}})

const defaultState = {}

let gqlReducer = (state, action) => {
  if (state === undefined) {
    return defaultState
  }
  if (action.type === 'LOGIN') {
    localStorage.authToken = action.token
    console.log("token  " + localStorage.authToken)
    return {token: action.token, data: jwt_decode(action.token)}
  }
  if(action.type === 'LOGOUT') {
    localStorage.removeItem('authToken')
    console.log("token  " + localStorage.authToken)
    return {}
  }
  if (action.type === 'REGISTER') {
    return {}
  }
  return state
}

const reducers = combineReducers({
  token: gqlReducer,
  promise: promiseReducer
})


const store = createStore(reducers, applyMiddleware(thunk))

const actionLogin = (token) => ({type: 'LOGIN', token})
const actionLogout = () => ({type: 'LOGOUT'})
const actionOnLogin = (log,passw) => actionPromiseLogin(log, passw)

const actionGetCatagories = () => actionPromise('getCategories', gql.request(`query categories {
  CategoryFind (query: "[{}]") {
    _id, name, goods{name},image{_id}
  }
}`))

const actionGetGoods = () => actionPromise('getGoods', gql.request(`query good {
  GoodFind(query: "[{}]") {
   name, _id, categories{name}, description, price, images{url, _id}, orderGoods{count, _id}
  }
}`))


store.subscribe(() => console.log(store.getState()))

export {store}
export {actionOnLogin}
export {actionGetCatagories}
export {actionGetGoods}
