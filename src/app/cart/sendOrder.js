import React from 'react';
import {actionSend} from '../../redux/reducers/actions.js';
import {connect}   from 'react-redux';


let SendOrder = (props) =>
    Object.keys(props.goodsData).length > 0 ?
        <button className='send-order-button'
                onClick={() => props.send(props.goodsData)}>
            Send Order
        </button> : ''

SendOrder = connect(state => ({
        data: state && state.promise
            && state.promise.sendOrder,
        goodsData: state && state.addGood
    }),
    {
        send: actionSend
    })
(SendOrder)

export {SendOrder}
