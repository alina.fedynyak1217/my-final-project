import React from 'react';
import {Link} from 'react-router-dom';
import {connect}   from 'react-redux';
import {actionGetGoods, actionDelete} from '../../redux/reducers/actions.js';
import {actionAdd, actionDel, delCart} from '../../redux/reducers/goodsCartReducer.js';
import {SendOrder} from './sendOrder.js';
import { Redirect } from 'react-router-dom';

let CartLink = () =>
    <Link className='cart' to='/cart'>
        <i className="fas fa-shopping-cart"></i>
    </Link>

const CartList = (props) => {
    return (
        <div className='cart-goods'>
            <span>{props.GoodName}</span>
            <button onClick={props.Dec}>-</button>
            <input value={props.count}
                   onChange={props.change}/>
            <button onClick={props.Inc}>+</button>
            <button onClick={props.Delete}>
                <i className="fas fa-trash-alt"></i>
            </button>
        </div>
    )
}

class Cart extends React.Component {

    render() {
        if (this.props.redirectData && this.props.redirectData.status === 'RESOLVED') {
            return (
                <Redirect to={`/orderData/${this.props.redirectData.payload.OrderUpsert._id}`}/>
            )
        } else {
            return (
                <div className='order-list'>
                    {Object.keys(this.props.goodsData).map(good => {
                            let orderItem
                            if (this.props.promiseData != null) {
                                this.props.promiseData.forEach(item => {
                                    if (item._id === good) {
                                        orderItem = item
                                    }
                                })
                            }
                            return <CartList key={good} GoodName={orderItem && orderItem.name}
                                             Dec={() => {
                                                 if (this.props.goodsData[good] > 1) {
                                                     this.props.incGood(good, this.props.goodsData[good] - 1)
                                                 }
                                             }}
                                             count={this.props.goodsData[good]}
                                             change={(e) => this.props.incGood(good, e.target.value)}
                                             Inc={() => this.props.incGood(good,
                                                 this.props.goodsData[good] + 1)}
                                             Delete={() => this.props.delGood(good)}/>
                        }
                    )}
                    <SendOrder/>
                </div>
            )
        }
    }

    componentDidMount() {
        this.props.onGoods()
    }

    componentWillUnmount() {
        if (this.props.redirectData && this.props.redirectData.status === 'RESOLVED') {
            this.props.unmount()
            this.props.cartDel()
        }
    }
}

Cart = connect(state => ({
        promiseData: state && state.promise
            && state.promise.getGoods
            && state.promise.getGoods.payload
            && state.promise.getGoods.payload.GoodFind,
        goodsData: state && state.addGood,
        redirectData: state && state.promise
            && state.promise.sendOrder
    }),
    {
        onGoods: actionGetGoods,
        incGood: actionAdd,
        delGood: actionDel,
        unmount: actionDelete,
        cartDel: delCart
    })
(Cart)

export {CartLink, Cart}
