import React from 'react';
import {connect}   from 'react-redux';
import {actionGetOrder} from '../../redux/reducers/actions.js';

let OrderCard = (props) =>
    <div>
        <p>
            <span>{props.name}</span><span> {props.count}</span>
            <span> {props.price} USD</span></p>
        <p>{props.amount} USD</p>
    </div>

class OrderData extends React.Component {

    render() {
        return (
            <div>
                <h3> Order № {this.props.orderData && this.props.orderData.createdAt}</h3>
                {this.props.orderData && this.props.orderData.orderGoods.map(item =>
                    <OrderCard key={item._id}
                               name={item.good.name}
                               count={item.count}
                               price={item.good.price}
                               amount={this.props.orderData && this.props.orderData.total}/>)}
            </div>
        )
    }

    componentDidMount() {
        this.props.getOrder(this.props.match.params.id)
    }
}

OrderData = connect(state => ({
        data: state && state.promise
            && state.promise.sendOrder
            && state.promise.sendOrder.payload
            && state.promise.sendOrder.payload.OrderUpsert
            && state.promise.sendOrder.payload.OrderUpsert,
        orderData: state && state.promise
            && state && state.promise.getOrder
            && state.promise.getOrder.payload
            && state.promise.getOrder.payload.OrderFindOne
    }),
    {
        getOrder: actionGetOrder
    })
(OrderData)

export {OrderData, OrderCard}
