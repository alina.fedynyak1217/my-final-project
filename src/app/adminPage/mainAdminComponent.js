import React from 'react';
import {Link} from 'react-router-dom';

const MainAdminPage = (props) =>
    <div className='main-admin'>
        <Link to='/admin/new-cat'> New category </Link>
        <Link to='/admin/new-good'> New good </Link>
        <Link to='/admin/new-img'> New image </Link>
    </div>

export {MainAdminPage}
