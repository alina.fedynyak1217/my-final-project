import React from 'react';
import {connect} from 'react-redux';
import {actionAddCategory} from '../../../redux/reducers/actions.js'

class AddCat extends React.Component {
  constructor(props) {
    super(props)
    this.state ={
      value: "",
    }
    this.onchange = this.onchange.bind(this)
  }
  onchange = () => e => this.setState({value: e.target.value})

  render() {
    return(
      <div>
        <input value={this.state.value}
               onChange= {this.onchange()}
               placeholder= 'enter categorie name'/>
        <button onClick = {() => this.props.addAction(this.state.value)}>Confirm</button>
      </div>
    )
  }
}

AddCat = connect(state => ({
    data: state && state.promise && state.promise.addCat}),
    {addAction:actionAddCategory}) (AddCat)

export {AddCat}
