import React from 'react';
import {connect} from 'react-redux';
import {actionGetCatagories, actionAddGood} from '../../../redux/reducers/actions.js'

class AddGood extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            description: '',
            price: '',
            category: '',
        }
        this.onchangeName = this.onchangeName.bind(this)
        this.onchangeDescription = this.onchangeDescription.bind(this)
        this.onchangePrice = this.onchangePrice.bind(this)
        this.onchangeCategory = this.onchangeCategory.bind(this)
    }

    onchangeName = () => e => this.setState({name: e.target.value})
    onchangeDescription = () => e => this.setState({description: e.target.value})
    onchangePrice = () => e => this.setState({price: e.target.value})
    onchangeCategory = () => e => this.setState({category: e.target.value})

    render() {
        return (
            <div>
                <input value={this.state.name}
                       onChange={this.onchangeName()}
                       placeholder='enter good name'/>
                <input value={this.state.price}
                       onChange={this.onchangePrice()}
                       placeholder='enter good price'/>
                <textarea value={this.state.description}
                          onChange={this.onchangeDescription()}
                          placeholder='enter good description'/>
                <select value={this.state.category}
                        onChange={this.onchangeCategory()}>
                    {this.props.selectData && this.props.selectData.map(item => <option
                        key={item._id}>{item.name}</option>)}
                </select>
                <button onClick={() => {
                    let id
                    this.props.selectData && this.props.selectData.forEach(item => {
                        if (item.name === this.state.category) {
                            id = item._id
                        }
                    })
                    this.props.addGood(this.state.name, this.state.description, +this.state.price, this.state.category, id)
                }}>
                    Confirm
                </button>
            </div>
        )
    }

    componentDidMount() {
        this.props.getCat()
    }
}

AddGood = connect(state => ({
        selectData: state && state.promise
            && state.promise.getCategories
            && state.promise.getCategories.payload
            && state.promise.getCategories.payload.CategoryFind,
        addData: state && state.promise && state.promise.addGood
    }),
    {
        getCat: actionGetCatagories,
        addGood: actionAddGood
    })
(AddGood)

export {AddGood}
