import React from 'react';
import {connect} from 'react-redux';
import {actionGetGoods, actionAddPhotoBack, actionAddImage} from '../../../redux/reducers/actions.js';

class AddImage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            goodValue: "",
        }

        this.onchangeGood = this.onchangeGood.bind(this)
        this.onchangePhoto = this.onchangePhoto.bind(this)
    }

    onchangeGood = e => this.setState({goodValue: e.target.value})
    onchangePhoto = () => this.props.upload(this.form)


    render() {
        return (
            <div>
                <form action='/upload' method='post' encType="multipart/form-data" ref={e => this.form = e}>
                    <input type='file' name="photo" onChange={this.onchangePhoto} id='photo'/>
                </form>
                <input type='text' list='goods' onChange={this.onchangeGood}/>
                <datalist id='goods'>
                    {this.props.goods && this.props.goods.map(item => <option key={item._id}>{item.name}</option>)}
                </datalist>
                <button onClick={() => {
                    let id
                    this.props.goods && this.props.goods.filter(good => {
                        if (good.name === this.state.goodValue) {
                            return id = good._id
                        }
                    })
                    this.props.addImg(this.props.uploadData && this.props.uploadData._id, id)
                }}>Submit
                </button>
            </div>
        )
    }

    componentDidMount() {
        this.props.getGoods()
    }
}

AddImage = connect(state => ({
        goods: state && state.promise
            && state.promise.getGoods
            && state.promise.getGoods.payload
            && state.promise.getGoods.payload.GoodFind,
        uploadData: state && state.promise && state.promise.UPLOAD
            && state.promise.UPLOAD.payload
    }),
    {
        getGoods: actionGetGoods,
        upload: actionAddPhotoBack,
        addImg: actionAddImage
    })
(AddImage)


export {AddImage}
