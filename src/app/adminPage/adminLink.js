import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux'

let AdminLink = (props) =>
    <>
        {props.data && props.data.includes('admin') ?
            <Link className='admin-link' to='/admin'>
                <i className="fas fa-tools"></i>
            </Link> : ''}
    </>

     AdminLink = connect(state => ({
        data: state && state.token
            && state.token.data
            && state.token.data.sub
            && state.token.data.sub.acl
    }))
    (AdminLink)

export {AdminLink}
