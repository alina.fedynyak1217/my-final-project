import React from 'react';
import {connect}   from 'react-redux';
import {actionGetOrderHistory} from '../../redux/reducers/actions.js';
import {Link} from 'react-router-dom';

let Order = (props) =>
    <div className='order-history-item'>
        <span>Order №: {props.orderNum}</span>
        <span> Order amount: {props.amount} USD</span>
        <span> Goods sum: {props.sum}</span>
    </div>

class OrderHistory extends React.Component {

    render() {
        return (
            <div className='order-history-container'>
                {this.props.orders && this.props.orders.map(item =>
                    <Link key={item._id} to={`/orderHistory/${this.props.match.params.id}/${item._id}`}>
                        <Order orderNum={item.createdAt} amount={item.total} sum={item.orderGoods.length}
                        />
                    </Link>
                )}
            </div>
        )
    }

    componentDidMount() {
        this.props.getOrders()
    }
}

OrderHistory = connect(state => ({
        orders: state && state.promise
            && state.promise.getOrderHistory
            && state.promise.getOrderHistory.payload
            && state.promise.getOrderHistory.payload.OrderFind
    }),
    {getOrders: actionGetOrderHistory})
(OrderHistory)

export {OrderHistory}
