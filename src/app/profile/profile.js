import React from 'react';
import {connect}   from 'react-redux';
import {Link} from 'react-router-dom';
import {actionGetUser} from '../../redux/reducers/actions.js'

let ProfileButton = (props) =>
    props.loggedData ? <Link className='profile-link' to='/user'>
        <i className="fas fa-user"></i>
    </Link> : ''

class Profile extends React.Component {

    render() {
        return (
            <div className='profile-page'>
                <p><span> Login:</span>
                    <span>{this.props.userData && this.props.userData.login}</span></p>
                <p><span> Nick:</span>
                    <span>{this.props.userData && this.props.userData.nick ? this.props.userData.nick : 'none'}</span>
                </p>
                <p><span> Date of profile creation:</span>
                    <span>{this.props.userData
                    && this.props.userData.createdAt
                    && new Date(+this.props.userData.createdAt).toLocaleString()}
            </span></p>
                <Link to={`/orderHistory/${this.props.userData && this.props.userData._id}`}>
                    Orders history
                </Link>
            </div>
        )
    }

    componentDidMount() {
        this.props.getUser(this.props.userId)
    }
}

ProfileButton = connect(state => ({
    loggedData: state && state.token && state.token.token
}))
(ProfileButton)
Profile = connect(state => ({
        userId: state && state.token
            && state.token.data
            && state.token.data.sub
            && state.token.data.sub.id,
        userData: state && state.promise
            && state.promise.getUser
            && state.promise.getUser.payload
            && state.promise.getUser.payload.UserFindOne
    }),
    {getUser: actionGetUser})
(Profile)

export {ProfileButton, Profile}
