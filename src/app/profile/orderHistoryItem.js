import React from 'react';
import {connect}   from 'react-redux';
import {actionGetOrderOne} from '../../redux/reducers/actions.js';

class OrderHistoryItem extends React.Component {

    render() {
        if (this.props.aclData && this.props.aclData.includes('admin')) {
            return (
                <div className='order-history-item-container'>
                    <h3>Order №: {this.props.getOrder && this.props.getOrder.createdAt}</h3>
                    <p>User: {this.props.getOrder && this.props.getOrder.owner && this.props.getOrder.owner.login}</p>
                    {this.props.getOrder && this.props.getOrder.orderGoods && this.props.getOrder.orderGoods.map(item =>
                        <div key={item && item._id}>
                            <p><span style={{paddingRight: '15px'}}>{item.good && item.good.name}</span>
                                <span style={{paddingRight: '5px'}}>{item && item.count + ' '}x</span>
                                <span style={{paddingRight: '5px'}}>{item.good && item.good.price + " "}USD</span></p>
                            <p style={{textAlign: 'right'}}>Total sum: {item.total}USD</p>
                        </div>
                    )}
                </div>
            )
        } else {
            return (
                <div className='order-history-item-container'>
                    <h3>Order №: {this.props.getOrder && this.props.getOrder.createdAt}</h3>
                    {this.props.getOrder && this.props.getOrder.orderGoods && this.props.getOrder.orderGoods.map(item =>
                        <div key={item._id}>
                            <p><span style={{paddingRight: '15px'}}>{item.good && item.good.name}</span>
                                <span style={{paddingRight: '5px'}}>{item.count + ' '}x</span>
                                <span style={{paddingRight: '5px'}}>{item.good && item.good.price + " "}USD</span></p>
                            <p style={{textAlign: 'right'}}>Total sum: {item.total}USD</p>
                        </div>
                    )}
                </div>
            )
        }
    }

    componentDidMount() {
        this.props.actionGet(this.props.match.params.id2)
    }
}

OrderHistoryItem = connect(state => ({
        getOrder: state && state.promise
            && state.promise.getOrderOne
            && state.promise.getOrderOne.payload
            && state.promise.getOrderOne.payload.OrderFindOne,
        aclData: state && state.token
            && state.token.data
            && state.token.data.sub
            && state.token.data.sub.acl
    }),
    {actionGet: actionGetOrderOne})
(OrderHistoryItem)

export {OrderHistoryItem}
