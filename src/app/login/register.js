import React from 'react';
import {connect}   from 'react-redux';
import {actionRegister} from '../../redux/reducers/actions.js';
import { Redirect } from 'react-router-dom';

class Register extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            login: '',
            password: '',
            confirmPassword: '',
            checkbox: 'password'
        }
        this.loginOnchange = this.loginOnchange.bind(this)
        this.passwordOnchange = this.passwordOnchange.bind(this)
        this.confirmPasswordOnchange = this.confirmPasswordOnchange.bind(this)
        this.onclick = this.onclick.bind(this)
        this.checkboxChange = this.checkboxChange.bind(this)
    }

    loginOnchange = () => e => this.setState({login: e.target.value})
    passwordOnchange = () => e => this.setState({password: e.target.value})
    confirmPasswordOnchange = () => e => this.setState({confirmPassword: e.target.value})
    onclick = () => {
        if (this.state.password === this.state.confirmPassword) {
            this.props.reg(this.state.login, this.state.confirmPassword)
        }
    }
    checkboxChange = () => this.state.checkbox === 'password' ? this.setState({checkbox: 'text'}) : this.setState({checkbox: 'password'})

    render() {
        if (this.props.regData && this.props.regData.UserUpsert && this.props.regData.UserUpsert._id) {
            return (
                <Redirect to='/sign-in'/>
            )
        } else {
            return (
                <div className='login-block'>
                    <input placeholder="Login"
                           value={this.state.login}
                           onChange={this.loginOnchange()}
                    />
                    <input placeholder="Password"
                           type={this.state.checkbox}
                           value={this.state.password}
                           onChange={this.passwordOnchange()}
                    />
                    <input placeholder="Repeat Password"
                           type={this.state.checkbox}
                           value={this.state.confirmPassword}
                           onChange={this.confirmPasswordOnchange()}
                    />
                    <div>
                        <input type='checkbox'
                               onChange={this.checkboxChange}/>
                        <span>Show password</span>
                    </div>
                    <button onClick={this.onclick}>Register</button>
                </div>
            )
        }

    }
}

Register = connect(state => ({
        regData: state && state.promise
            && state.promise.REGISTER && state.promise.REGISTER.payload
    }),
    {
        reg: actionRegister
    })
(Register)

export {Register}
