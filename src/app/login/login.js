import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import {connect}   from 'react-redux';
import {actionOnLogin, actionLogout, actionDeleteRegister} from '../../redux/reducers/actions.js';

let SignIn = (props) =>
    props.loggedData ? <button className='sign-up'
                               onClick={() => {
                                   props.logout();
                                   props.delReg()
                               }}>
            <i className='fas fa-sign-out-alt'></i>
        </button> :
        <Link className='sign-in' to='/sign-in'>
            <i className="fas fa-sign-in-alt"></i>
        </Link>

class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            login: '',
            password: '',
            checkbox: 'password'
        }
        this.loginOnchange = this.loginOnchange.bind(this)
        this.passwordOnchange = this.passwordOnchange.bind(this)
        this.checkboxChange = this.checkboxChange.bind(this)
    }

    loginOnchange = () => e => this.setState({login: e.target.value})
    passwordOnchange = () => e => this.setState({password: e.target.value})
    checkboxChange = () => this.state.checkbox === 'password' ? this.setState({
        checkbox: 'text'
    }) : this.setState({checkbox: 'password'})

    render() {
        if (this.props.logged) {
            return (
                <Redirect to='/'/>
            )
        } else {
            return (
                <div className='login-block'>
                    <input placeholder='Login'
                           type='text'
                           value={this.state.login}
                           onChange={this.loginOnchange()}
                    />
                    <input placeholder='Password'
                           type={this.state.checkbox}
                           value={this.state.password}
                           onChange={this.passwordOnchange()}
                    />
                    <div>
                        <input type='checkbox'
                               onChange={this.checkboxChange}
                        />
                        <span>Show password</span>
                    </div>
                    <div>
                        <button onClick={() => this.props.onLogin(this.state.login, this.state.password)}>
                            Login
                        </button>
                        <Link to='/register'>Registration</Link>
                    </div>
                </div>
            )
        }
    }

}

SignIn = connect(state => ({
        loggedData: state && state.token && state.token.token
    }),
    {logout: actionLogout, delReg: actionDeleteRegister})
(SignIn)

Login = connect(state => ({
        promiseData: state && state.promise && state.promise.LOGIN,
        logged: state && state.token && state.token.token
    }),
    {onLogin: actionOnLogin})
(Login)

export {Login, SignIn}
