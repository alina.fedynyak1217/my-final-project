import React from 'react';
import {GoodItems} from '../goodsmenu/goodsList.js';
import {connect}   from 'react-redux';
import {Link} from 'react-router-dom';
import {actionSearch} from '../../../redux/reducers/actions.js';

class SearchResults extends React.Component {

    render() {
        if (this.props.data && this.props.data.length === 0) {
            return (
                <div>
                    No results by your query
                </div>
            )
        } else {
            return (
                <div className='good-list-container'>
                    {this.props.data && this.props.data.map(good => good.categories !== null && good.categories.length > 0 ?
                        <Link className='goods-list-item' key={good._id}
                              to={`/good/${good.categories[0]._id}/${good._id}`}>
                            <GoodItems
                                img={good.images != null && good.images[0] && good.images[0].url ? "http://shop-roles.asmer.fs.a-level.com.ua/"
                                    + (good.images != null && good.images[0] && good.images[0].url) : ""}
                                name={good.name} price={good.price}/>
                        </Link> : '')}
                </div>
            )
        }
    }
}


SearchResults = connect(state => ({
        data: state && state.promise
            && state.promise.searchGood
            && state.promise.searchGood.payload
            && state.promise.searchGood.payload.GoodFind
    }),
    {search: actionSearch})
(SearchResults)

export {SearchResults}
