import React from 'react';
import {connect}   from 'react-redux';
import {actionSearch} from '../../../redux/reducers/actions.js';
import {Link } from 'react-router-dom';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '300px',
        height: '100px'
    }
};

class SearchBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            query: ''
        };

        this.onChange = this.onChange.bind(this)
    }

    onChange = e => this.setState({query: e.target.value})

    render() {
        return (
            <div className="search-bar">
                <form>
                    <input className='search-input'
                           value={this.state.query}
                           onChange={this.onChange}
                           placeholder='search'/>
                    <Link className='search-link' to='/search-results'
                          onClick={() => {
                              this.props.search(this.state.query)
                              this.setState({modalIsOpen: false, query: ""})
                          }}>
                        <i className="fas fa-search"></i>
                    </Link>
                </form>
            </div>
        );
    }
}

SearchBar = connect(state => ({
        data: state && state.promise
            && state.promise.searchGood
            && state.promise.searchGood.payload
            && state.promise.searchGood.payload.GoodFind
    }),
    {search: actionSearch})
(SearchBar)

export {SearchBar}
