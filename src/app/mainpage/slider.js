import AwesomeSlider from 'react-awesome-slider';
import AwsSliderStyles from 'react-awesome-slider/src/styles.scss';

const Slider = (props) =>
    <div>
        <AwesomeSlider cssModule={AwsSliderStyles}>
            <div data-src="/path/to/image-0.png"/>
            <div data-src="/path/to/image-1.png"/>
            <div data-src="/path/to/image-2.jpg"/>
        </AwesomeSlider>
    </div>

export {Slider}
