import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {actionGetGoods} from '../../redux/reducers/actions.js';

class MainPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            position: 0,
            src: "http://shop-roles.asmer.fs.a-level.com.ua/images/00505f5f08ac113874318dee67975aa9",
            good: {_id: "5dc45d0b5df9d670df48cc4b", categories: [{_id: "5dc458985df9d670df48cc47"}]}
        }
        this.setPosition = this.setPosition.bind(this)
        this.start = this.start.bind(this)
    }

    setPosition(pos) {
        let position = 0
        if (this.props.dataImg !== null) {
            position = pos >= this.props.dataImg.length ? 0 : pos
        }


        this.setState({
            src: "http://shop-roles.asmer.fs.a-level.com.ua/" + this.props.dataImg
                && this.props.dataImg[this.state.position]
                && this.props.dataImg[this.state.position].images !== null
                && this.props.dataImg[this.state.position].images[0]
                && this.props.dataImg[this.state.position].images[0].url
                || "http://shop-roles.asmer.fs.a-level.com.ua/images/00505f5f08ac113874318dee67975aa9",
            good: this.props.dataImg[this.state.position], position
        })
    }

    start() {
        this.intervalId = setInterval(() => this.setPosition(this.state.position + 1), 5000)
    }

    render() {
        return (
            <div style={{width: '100%', height: '60vh', overflow: 'hidden'}}>
                <Link to={`/good/${this.state.good && this.state.good.categories
                && this.state.good.categories.length !== 0
                && this.state.good.categories[0]._id
                || 'no-category'}/${this.state.good && this.state.good._id}`}>
                    <img style={{width: '100%', height: '100%'}} alt='' src={this.state.src}
                    />
                </Link>
            </div>
        )
    }

    componentDidMount() {
        this.start()
        this.props.getGoods()
    }

    componentWillUnmount() {
        clearInterval(this.intervalId)
    }
}

MainPage = connect(state => ({
        dataImg: state && state.promise
            && state.promise.getGoods
            && state.promise.getGoods.payload
            && state.promise.getGoods.payload.GoodFind
    }),
    {getGoods: actionGetGoods})
(MainPage)

export {MainPage}
