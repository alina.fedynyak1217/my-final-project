import React from 'react';
import {connect}   from 'react-redux';
import {actionGetCatagorieOne} from '../../../redux/reducers/actions.js';
import {Link} from 'react-router-dom';

let GoodItems = (props) =>
    <div>
        <img alt='' src={props.img}/>
        <h4> {props.name} </h4>
        <span>{props.price + 'USD'} </span>
    </div>

class GoodsList extends React.Component {

    render() {
        return (
            <div className='good-list-container'>
                {this.props.promiseData && this.props.promiseData.map(good =>
                    <Link className='goods-list-item' key={good._id}
                          to={`/good/${this.props.match.params.id}/${good._id}`}>
                        <GoodItems
                            img={good.images != null && good.images[0] && good.images[0].url ? "http://shop-roles.asmer.fs.a-level.com.ua/"
                                + (good.images != null && good.images[0] && good.images[0].url) : ""}
                            name={good.name} price={good.price}
                        />
                    </Link>
                )}

            </div>
        )
    }

    componentDidMount() {
        this.id = this.props.match.params.id
        this.props.onGoods(this.id)

    }

    componentDidUpdate() {
        if (this.id !== this.props.match.params.id) {
            this.componentDidMount()
        }
    }
}

GoodsList = connect(state => ({
        promiseData: state && state.promise
            && state.promise.getCategorieOne
            && state.promise.getCategorieOne.payload
            && state.promise.getCategorieOne.payload.CategoryFindOne
            && state.promise.getCategorieOne.payload.CategoryFindOne.goods
    }),
    {onGoods: actionGetCatagorieOne})
(GoodsList)

export {GoodsList, GoodItems}
