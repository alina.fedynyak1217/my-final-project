import React from 'react';
import {actionGetGoods} from '../../../redux/reducers/actions.js';
import {connect}   from 'react-redux';
/*import {actionGetGoods} from '../../../store.js';*/
import {actionAdd} from '../../../redux/reducers/goodsCartReducer.js'

class GoodCard extends React.Component {

    render() {
        let good = {}

        this.props.promiseData != null && this.props.promiseData.forEach(item => {
            if (item._id === this.props.match.params.id2) {
                good = item
            }
        })
        return (
            <div className='good-card'>
                <h3> {good.name} </h3>
                <div>
                    {good.images && good.images != null && good.images[0] && good.images[0].url ?
                        <img alt=''
                             src={good.images != null && good.images[0] && good.images[0].url ? "http://shop-roles.asmer.fs.a-level.com.ua/"
                                 + (good.images && good.images[0] && good.images[0].url) : ""}
                        /> : <img alt=''/>}

                    <p> {good.description} </p>
                </div>
                <div className='price-block'>
                    <span> {good.price} USD</span>
                    <button onClick={() => {
                        if (Object.keys(this.props.goodsData).includes(good._id)) {
                            this.props.add([good._id], this.props.goodsData[good._id] += 1)
                        } else {
                            this.props.add([good._id], 1)
                        }
                    }}>Buy
                    </button>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.props.onGoods()
    }
}

GoodCard = connect(state => ({
        promiseData: state && state.promise
            && state.promise.getGoods
            && state.promise.getGoods.payload
            && state.promise.getGoods.payload.GoodFind,
        goodsData: state && state.addGood
    }),
    {onGoods: actionGetGoods, add: actionAdd})
(GoodCard)

export {GoodCard}
