import React from 'react';
import {actionGetCatagories} from '../../../redux/reducers/actions.js'
import {connect}   from 'react-redux';
import {Link} from 'react-router-dom';

class MenuUl extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <div className='menu-container'>
                {this.props.promiseData && this.props.promiseData.CategoryFind.map(item =>
                    <Link key={item._id} to={`/good/${item._id}`}> {item.name}
                    </Link>
                )}
            </div>
        )
    }

    componentDidMount() {
        this.props.onCat()
    }
}

MenuUl = connect(state => ({
        promiseData: state && state.promise
            && state.promise.getCategories
            && state.promise.getCategories.payload
    }),
    {onCat: actionGetCatagories})
(MenuUl)

export {MenuUl}
