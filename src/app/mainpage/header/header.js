import React from 'react';
import {Link} from 'react-router-dom';


let Logo = () =>
    <Link className='logo' to='/'>
        <div className='logo-img'></div>
    </Link>

let Select = () =>
    <select>
        <option>Kharkov</option>
        <option>Kiev</option>
        <option>Odessa</option>
        <option>Dnepr</option>
        <option>Lvov</option>
        <option>Dnepr</option>
        <option>Odessa</option>
    </select>

let About = () =>
    <Link to='/about'>About</Link>

let Contacts = () =>
    <Link to='/contacts'>Contacts</Link>

let Blogs = () =>
    <Link to='/blog'>Blog</Link>

let Payments = () =>
    <Link to='/payment'>Payment</Link>

let Deliverys = () =>
    <Link to='/delivery'>Delivery</Link>


export {Logo, About, Blogs, Contacts, Select, Payments, Deliverys}
