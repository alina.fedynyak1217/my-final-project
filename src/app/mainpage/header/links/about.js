import React from 'react';

const AboutUs = () => {
    return (
        <div className='about-block'>
            <img src='https://www.qeretail.com/blog/wp-content/uploads/2017/08/online_shopping.png' alt='content'/>
            <h3>Спасибо Вам за Ваш выбор! Если вы читаете эти слова, значит вы решили
                узнать нас чуть-чуть лучше. Кратко,
                о том, что может вас заинтересовать:</h3>
            <p>Мы на рынке с 2021 года</p>
            <p>Офис расположен в Харькове, но работаем мы, конечно же, по всей Украине</p>
            <p>Наш свой собственный склад позволяет сократить время доставки товара после его оплаты до 1-2 дней</p>
            <p>В нашем магазине представлены товары известных всему миру производителей: Luminarc, Krauff, BergHOFF,
                Art Craft, Wellberg, Everglass, Benson, Edco, Arcopal, Arcoroc, Rondell, Keramia, Tramontina, Herevin,
                Bager, Vissner, Milika, Vincent, Helfer, Limited Edition, Elbee, Vinzer, Berlinger Haus, Blaumann,
                Bergner, Lessner, Pixel, Wilmax, Zurrichberg, Rainstahl, Pyrex, Edenberg, Con Brio, Fissman, Empire,
                Ardesto, Gusto, Maxmark,
                Renberg, San Ignacio, Peterhof, Bohmann, Westhill, Ipec, Ringel, Qlux, TVS, Bohemia, Maestro, Idilia
                Новомосковская посуда, Prolis, Биол и др. .</p>
            <p>Весь товар сертифицирован</p>
            <p>Теперь несколько подробнее: Одно из основных наших преимуществ — широкий ассортимент, в нашем каталоге
                для себя найдут необходимые товары как домохозяйки так и представители отельно-ресторанных комплексов,
                кафе, клубов, пиццерий, хостелов.</p>
        </div>
    )
}

export { AboutUs }
