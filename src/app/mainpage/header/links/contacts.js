import React from 'react';

const Contact = () => {
    return (
        <div className="about-block">
            <img src="https://1gai.ru/uploads/posts/2014-12/1418145926_5.jpg" alt=""/>
            <div className='contact-block'>
                <div>
                    <h3>Контакты магазина</h3>
                    <ul>
                        <li>45-5566-666</li>
                        <li>45-5566-666</li>
                        <li>45-5566-666</li>
                        <li>45-5566-666</li>
                    </ul>
                </div>
                <div>
                    <h3>Адрес:</h3>
                    <p>проспект Льва Ландау, Харьков, 61110</p>
                </div>
            </div>
        </div>
    );
}
export { Contact }
