import React from 'react';
import './App.css';
import {Provider}   from 'react-redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Logo, About, Blogs, Contacts, Select, Payments, Deliverys} from './app/mainpage/header/header.js';
import {AboutUs} from './app/mainpage/header/links/about.js'
import {Contact} from './app/mainpage/header/links/contacts'
import {Delivery} from './app/mainpage/header/links/delivery'
import {Payment} from './app/mainpage/header/links/payment'
import {Blog} from "./app/mainpage/header/links/blog";
import {SearchBar} from './app/mainpage/searchbar/search.js'
import {MenuUl} from './app/mainpage/goodsmenu/goodsMenu.js'
import store from './redux/store.js';
import {PrivateRoute} from './redux/reducers/privateRoute.js'
import {Login, SignIn} from './app/login/login.js'
import {GoodsList} from './app/mainpage/goodsmenu/goodsList.js'
import {GoodCard} from "./app/mainpage/goodsmenu/goodCard";
import {CartLink, Cart} from './app/cart/cart.js';
import {OrderData} from './app/cart/orderData.js';
import {ProfileButton, Profile} from './app/profile/profile.js';
import {OrderHistory} from './app/profile/orderHistory.js';
import {OrderHistoryItem} from './app/profile/orderHistoryItem.js';
import {Register} from './app/login/register.js'
import {SearchResults} from './app/mainpage/searchbar/searchResults.js'
import {MainAdminPage} from './app/adminPage/mainAdminComponent.js';
import {AddCat} from './app/adminPage/adminActions/addCategorie.js'
import {AddGood} from './app/adminPage/adminActions/addGood.js'
import {AddImage} from './app/adminPage/adminActions/addImage'
import {AdminLink} from './app/adminPage/adminLink.js';
import {MainPage} from './app/mainpage/mainpage.js';

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
          <div className="App">
              <div className='header'>
                  <nav className='Navigator'>
                      <Logo/>
                      <Select/>
                      <div className='mainlinks'>
                          <About/>
                          <Contacts/>
                          <Blogs/>
                          <Payments/>
                          <Deliverys/>
                      </div>
                      <AdminLink/>
                      <SignIn/>
                      <ProfileButton/>
                      <CartLink/>
                  </nav>
              </div>
              <div>
                  <SearchBar/>
                  <div className='categories-and-goods-wrapper'>
                      <MenuUl/>
                      <div className='switch-container'>
                          <Switch>
                              <Route path="/about" exact component={AboutUs}/>

                              <Route path="/contacts" exact component={Contact}/>
                              <Route path="/blog" exact component={Blog}/>
                              <Route path="/payment" exact component={Payment}/>
                              <Route path="/delivery" exact component={Delivery}/>
                              <PrivateRoute fallback='/sign-in' roles={['user']} path="/cart" exact component={Cart}/>
                              <Route path="/good/:id" exact component={GoodsList}/>
                              <Route path="/good/:id/:id2" exact component={GoodCard}/>
                              <Route path="/sign-in" exact component={Login}/>
                              <Route path="/register" exact component={Register}/>
                              <PrivateRoute fallback='/sign-in' roles={['user']} path="/orderData/:id" exact
                                            component={OrderData}/>
                              <Route path="/" exact component={MainPage}/>
                              <PrivateRoute fallback='/sign-in' roles={['user']} path="/orderHistory/:id" exact
                                            component={OrderHistory}/>
                              <PrivateRoute fallback='/sign-in' roles={['user']} path="/user" exact
                                            component={Profile}/>
                              <PrivateRoute fallback='/sign-in' roles={['user']} path="/orderHistory/:id/:id2" exact
                                            component={OrderHistoryItem}/>
                              <Route path="/search-results" exact component={SearchResults}/>
                              <PrivateRoute fallback='/sign-in' roles={['admin']} path="/admin" exact
                                            component={MainAdminPage}/>
                              <PrivateRoute fallback='/sign-in' roles={['admin']} path="/admin/new-cat" exact
                                            component={AddCat}/>
                              <PrivateRoute fallback='/sign-in' roles={['admin']} path="/admin/new-good" exact
                                            component={AddGood}/>
                              <PrivateRoute fallback='/sign-in' roles={['admin']} path="/admin/new-img" exact
                                            component={AddImage}/>
                          </Switch>
                      </div>
                  </div>
              </div>
          </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
